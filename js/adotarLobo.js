const clientUrl = window.location.href;
const apiUrl = "http://lobinhos.herokuapp.com";
const form = document.querySelector(".adopter-info");

const paramString = clientUrl.split('?')[1];
const queryString = new URLSearchParams(paramString);
const id = queryString.get('adotar');

const wolfImg = document.querySelector('.wolf-img');
const wolfName = document.querySelector('.wolf-name');
const wolfIdTag = document.querySelector('.wolf-id');
fetch(apiUrl + "/wolves/" + id)
    .then(response => {
        if(response.err){
            alert(response.status);
        } else{
            return response.json();
        }
    })
    .then(wolf => {
        console.log(wolf);
        wolfImg.src = wolf.image_url
        wolfName.innerText = `Adote o(a) ${wolf.name}`
        wolfIdTag.innerText = `Id: ${wolf.id}`;
    })
    .catch(err => {
        alert("Error: " + err.message);
    })


form.addEventListener("submit", (event)=>{    
    const idade = parseInt(form.elements['age'].value.trim())
    if(isNaN(idade) || idade < 0) {
        //Por algum motivo esse blco não funciona
        alert("O campo 'idade' deve conter um inteiro positivo!");
        event.preventDefault();
        return;
    } 

    if(form.elements['name'].value.length === 0) {
        alert("O campo 'Nome' não pode se vazio!");
        event.preventDefault();
        return;
    }

    if(form.elements['email'].value.length === 0) {
        alert("O campo 'Nome' não pode se vazio!");
        event.preventDefault();
        return;
    }

    const body = {
        adopter_name: form.elements['name'].value,
        adopter_age: idade,
        adopter_email: form.elements['email'].value
    }

    console.log(body);

    const config = {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {"Content-type": "application/json"},
    }

    console.log(apiUrl + "/wolves/" + id);

    fetch(apiUrl + "/wolves/" + id, config)
    .then(response => {
        if(response.ok){
            alert("Success!");
        } else{
            alert(response.status);
        }
    })
    .catch(err => {
        alert("Error: " + err.message);
    })

    alert("");//Esse alert permite que a gente veja os outros alerts antes da página recarregar
    window.location.href = "../index.html";
})




console.log(id);
