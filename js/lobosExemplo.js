const url = "http://lobinhos.herokuapp.com";
const wolf1 = document.querySelector("#wolf-1");

const wolf2 = document.querySelector("#wolf-2");

function getWolfTest(wolfId){
    fetch(url + "/wolves/" + wolfId)
    .then(response => {
        if(response.ok){
            return response.json();
        } else if(response.status === 404){
            //Caso o ID não exista, o sistema deve nos retornar um erro 404
            //Nesse caso, vamos gerar um novo número aleátorio até termos um retorno correto
            console.log("error 404");
            return getWolf(wolfId);
        } else{
            return Promise.reject("Outro erro: " + response.status);
        }
    })
    .then(fetchedWolf => {
        const wolf = document.querySelector("#wolf-1");
        wolf.querySelector(".wolf-image").src = fetchedWolf.image_url;
        wolf.querySelector(".wolf-name").innerText = fetchedWolf.name;
        wolf.querySelector(".wolf-age").innerText = `Idade: ${fetchedWolf.age} anos`;
        wolf.querySelector(".wolf-description").innerText = fetchedWolf.description;
    });
}


function getWolf(wolfElem){
    /*Número aleatorio de 1-250*/
    random_id = Math.floor(Math.random() * 251) + 1;

    fetch(url + "/wolves/" + random_id)
    .then(response => {
        if(response.ok){
            return response.json();
        } else if(response.status === 404){
            //Caso o ID não exista, o sistema deve nos retornar um erro 404
            //Nesse caso, vamos gerar um novo número aleátorio até termos um retorno correto
            console.log("error 404");
            return getWolf(wolfElem);
        } else{
            return Promise.reject("Outro erro: " + response.status);
        }
    })
    .then((fetchedWolf) =>{
        wolfElem.querySelector(".wolf-image").src = fetchedWolf.image_url;
        wolfElem.querySelector(".wolf-name").innerText = fetchedWolf.name;
        wolfElem.querySelector(".wolf-age").innerText = `Idade: ${fetchedWolf.age} anos`;
        wolfElem.querySelector(".wolf-description").innerText = fetchedWolf.description;
    })
    .catch(err => {
        console.log("error: " + err);
    })
}

getWolf(wolf1);
getWolf(wolf2);